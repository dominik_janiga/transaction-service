# Serwis transakcyjny

## Spis treści
* [Opis zadania](#opis-zadania)
* [Uruchomienie aplikacji](#uruchomienie-aplikacji)
* [Przykład zapytania](#przykład-zapytania)
* [Przykład odpowiedzi](#przykład-odpowiedzi)
* [Technologie](#technologie)

## Opis zadania

W pewnym dużym banku, każdego dnia, przeprowadzanych są tysiące transakcji na rachunkach. 
Są to zarówno przelewy przychodzące (wpływy) jak i wychodzące (wydatki).

Jeden z działów banku chciałby wiedzieć, jak zmienia się saldo na wszystkich rachunkach po przetworzeniu transakcji z całego dnia. 
Dodatkowo ma wymaganie, aby lista rachunków była posortowana rosnąco.

Przewiduje się, że w niedalekiej przyszłości, takich transakcji może być nawet 100 000, ponieważ stale powiększa się jego baza klientów. 
Nasz system musi być na to gotowy!

Napisz algorytm, który przetworzy wszystkie transakcji i zwróci posortowaną listę rachunków, wraz z ilością uznań i obciążeń oraz saldo końcowe.
Zakłada się, że saldo początkowe każdego rachunku to 0zł.

## Uruchomienie aplikacji
```
git clone https://gitlab.com/dominik_janiga/transaction-service.git
cd transaction-service
bash build.sh
bash run.sh
```

## Przykład zapytania

```
[
  {
    "debitAccount": "32309111922661937852684864",
    "creditAccount": "06105023389842834748547303",
    "amount": 10.90
  },
  {
    "debitAccount": "31074318698137062235845814",
    "creditAccount": "66105036543749403346524547",
    "amount": 200.90
  },
  {
    "debitAccount": "66105036543749403346524547",
    "creditAccount": "32309111922661937852684864",
    "amount": 50.10
  }
]
```

## Przykład odpowiedzi

```
[
  {
    "account": "06105023389842834748547303",
    "debitCount": 0,
    "creditCount": 1,
    "balance": 10.90
  },
  {
    "account": "31074318698137062235845814",
    "debitCount": 1,
    "creditCount": 0,
    "balance": -200.90
  },
  {
    "account": "32309111922661937852684864",
    "debitCount": 1,
    "creditCount": 1,
    "balance": 39.20
  },
  {
    "account": "66105036543749403346524547",
    "debitCount": 1,
    "creditCount": 1,
    "balance": 150.80
  }
]

```
## Technologie

Java 17

Spring Boot 3.0.5

Maven 3.9.0
